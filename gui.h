//    Hannah, GUI for downloading specific printer firmware
//    Copyright (C) 2007 Steffen Joeris <white@debian.org>
//    Copyright (C) 2017 Pino Toscano <pino@debian.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License along
//    with this program; if not, write to the Free Software Foundation, Inc.,
//    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#ifndef GUI_H
#define GUI_H

#include <QHash>
#include <QProcess>
#include <QWidget>

class QAbstractButton;
class QButtonGroup;
class QLabel;
class QPushButton;

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);

private:
    void createMainWidget();
    QHash<QAbstractButton *, QString> firmwareMappings;
    QButtonGroup *buttonGroup;
    QPushButton *downloadButton;
    QPushButton *exitButton;
    QLabel *statusLabel;
    QProcess callProgram;
    QString string;

private Q_SLOTS:
    void callGetweb();
    void slotMarkAll();
    void slotUnmarkAll();
    void showInfo();
    void checkProgram(int);
};

#endif // GUI_H
